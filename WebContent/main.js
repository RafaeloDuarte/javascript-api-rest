populaClasse();
populaEnviroment();

// 'http://10.82.248.107:8081/bancodemassa-ui/ListOfEnvironmentType.json'
// 'http://10.82.248.107:8081/bancodemassa-ui/ListOfContentType.json'
// 'http://10.82.248.107:8081/bancodemassa-ws-node/classesDeMassa/'
// 'http://10.82.248.107:8081/bancodemassa-ws-cluster/cluster/'

function populaEnviroment() {

	var ourRequest = new XMLHttpRequest();
	ourRequest.open('GET',
			'http://10.82.248.107:8081/bancodemassa-ui/ListOfEnvironmentType.json');

	ourRequest.onload = function() {
		var ourData = JSON.parse(ourRequest.responseText);
		var concat = "";
		if (ourData.length == 0)
			document.getElementById("environmentType").innerHTML = "<option></option>";
		else {

			var opList = new Array();
			for (var i = 0; i < ourData.length; i++) {
				opList[i] = ourData[i].cluster_environment_type;
			}
			opList.sort();
			for (var i = 0; i < opList.length; i++) {
				concat += "<option>" + opList[i] + "</option>";
			}
			document.getElementById("environmentType").innerHTML = concat;

		}

	};
	ourRequest.send();
}

function populaClasse() {

	var ourRequest = new XMLHttpRequest();
	ourRequest.open('GET',
			'http://10.82.248.107:8081/bancodemassa-ui/ListOfContentType.json');
	ourRequest.onload = function() {
		var ourData = JSON.parse(ourRequest.responseText);
		var concat = "";
		if (ourData.length == 0)
			document.getElementById("massType").innerHTML = "<option></option>";
		else {

			var opList = new Array();
			for (var i = 0; i < ourData.length; i++) {
				opList[i] = ourData[i].cluster_content_type;
			}
			opList.sort();
			for (var i = 0; i < opList.length; i++) {
				concat += "<option>" + opList[i] + "</option>";
			}

			document.getElementById("massType").innerHTML = concat;

		}

	};
	ourRequest.send();
}

var btn = document.getElementById("btn");
var counter = 0;
var environmentType = "";
var massClass = "";
var massType = "";

function changecat(value) {

	massType = document.getElementById("massType").value;
	var ourRequest = new XMLHttpRequest();
	ourRequest.open('GET',
			'http://10.82.248.107:8081/bancodemassa-ws-node/classesDeMassa/'
					+ massType);

	// 'http://10.82.248.107:8081/bancodemassa-ws-node/classesDeMassa/ETL'
	// 'http://10.82.248.107:8081/bancodemassa-ws-node/classesDeMassa/PIPELINE'
	// 'http://10.82.248.107:8081/bancodemassa-ui/ListOfClasseMassa'+massType+'.json'

	ourRequest.onload = function() {
		var ourData = JSON.parse(ourRequest.responseText);
		renderHTML(ourData);
		// renderHTML(elemento);
	};
	ourRequest.send();

}

function carregaEnvironmentType(value) {
	environmentType = document.getElementById("environmentType").value;
}

function carregaMassClass(value) {
	massClass = document.getElementById("massClass").value;
}

var jsonUi = {
	"node" : {
		"environment_type" : environmentType,
		"content_type" : massType
	},
	"get" : {
		"classe_massa" : massClass
	},
	"controle" : {
		"reuso" : 0
	}
}

btn
		.addEventListener(
				"click",
				function() {

					if (environmentType.length == 0) {
						alert("Nenhum valor foi atribuido para Environment Type!");
					}
					if (massType.length == 0) {
						alert("Nenhum valor foi atribuido para Content Type!");
					}
					if (massClass.length == 0) {
						alert("Nenhum valor foi atribuido para Mass Class!");
					} else {
						var ourRequest = new XMLHttpRequest();
						ourRequest
								.open('GET',
										'http://10.82.248.107:8081/bancodemassa-ui/ListOfContentType.json');
						ourRequest.onload = function() {
							var ourData = JSON.parse(ourRequest.responseText);
							if (massType == 'PIPELINE') {
								massType = 'STAGE-PIPELINE'
							}
							jsonUi = {
								"node" : {
									"environment_type" : environmentType,
									"content_type" : massType
								},
								"get" : {
									"classe_massa" : massClass
								},
								"controle" : {
									"reuso" : 0
								}
							}
							renderHTMLJson(jsonUi);
							var jsonInLine = 'http://10.82.248.107:8081/bancodemassa-ws-cluster/cluster/'
									+ JSON.stringify(jsonUi);
							var newRequest = new XMLHttpRequest();
							newRequest.open('GET', jsonInLine);
							newRequest.onload = function() {
								var jsonFinal = JSON
										.parse(newRequest.responseText);
								renderHTMLJson3(jsonFinal);
								renderHTMLJson4(jsonFinal);

							};

							renderHTMLJson2(jsonInLine);
							newRequest.send();

							// renderHTML(elemento);
						};
						ourRequest.send();

					}
				})

function renderHTML(data) {

	var concat = "";

	if (data.length == 0)
		document.getElementById("massClass").innerHTML = "<option></option>";
	else {
		data.sort();
		for (var i = 0; i < data.length; i++) {
			concat += "<option>" + data[i] + "</option>";
		}
		document.getElementById("massClass").innerHTML = concat;

	}

}

function renderHTMLJson(data) {

	// document.getElementById("jsonRequisicao").innerHTML = JSON.stringify(jsonUi, undefined, 2);

}

function renderHTMLJson2(data) {

	// document.getElementById("urldeRequisicao").innerHTML =
	// JSON.parse(JSON.stringify(data, undefined, 2));

}

function renderHTMLJson3(data) {

	// document.getElementById("jsonResposta").innerHTML = JSON.stringify(data,
	// undefined, 2);

}

function renderHTMLJson4(data) {

	var urlResposta = data.node_return.url_rest_service;
	var urlRespostaString = JSON.stringify(data).toString().replace("/", "%30")
			.replace("/", "%30").replace("/", "%30").replace("/", "%30")
			.replace("/", "%30").replace("/", "%30").replace("/", "%30")
			.replace("/", "%30");
	// document.getElementById("urlResposta").innerHTML =
	// urlResposta+"/dados/"+urlRespostaString;
	renderHTMLJson5(urlResposta + "/dados/" + urlRespostaString);
	// renderHTMLJson5(urlResposta+urlRespostaString);

}

function renderHTMLJson5(data) {
	var lastRequest = new XMLHttpRequest();
	lastRequest.open('GET', data);
	lastRequest.onload = function() {
		var jsonFinalFinal = JSON.parse(lastRequest.responseText);
		document.getElementById("jsonRespostaFinal").innerHTML = JSON
				.stringify(jsonFinalFinal, undefined, 2);

		document.getElementById("sistema").innerHTML = (jsonFinalFinal.get_return.data.massa.sistema)
				.replace("%92", "");
		document.getElementById("loginName").innerHTML = (jsonFinalFinal.get_return.data.massa.login_name);
		document.getElementById("loginPassword").innerHTML = (jsonFinalFinal.get_return.data.massa.login_password);
		document.getElementById("environmentTypeCode").innerHTML = (jsonFinalFinal.get_return.data.massa.cod_tipo_ambiente);
		document.getElementById("Obs").innerHTML = (jsonFinalFinal.get_return.data.massa.obs);
		if (jsonFinalFinal.get_return.data.massa.obs.length == 0) {
			document.getElementById("Obs").innerHTML = 'Nenhuma a considerar';
		}

	};
	lastRequest.send();

}
