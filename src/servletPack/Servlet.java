package servletPack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

@WebServlet("/Servlet")
public class Servlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2967983374519342287L;

	public Servlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());

		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.println("Foo");
		pw.flush();
		pw.close();

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		StringBuilder json = new StringBuilder();
		BufferedReader reader = request.getReader();

		PrintWriter out = response.getWriter();

		out.println("");

		String json1 = new Gson().toJson(json);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(json1);

		doGet(request, response);
	}

	private String generateJsonData(String obj) {

		StringBuffer returnData = null;

		return null;
	}

}
